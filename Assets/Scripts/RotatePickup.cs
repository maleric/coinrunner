﻿using UnityEngine;
using System.Collections;

public class RotatePickup : MonoBehaviour {
	public int x = 0;
	public int y = 45;
	public int z = 0;
	public int speed = 4;

	void Update () {
		transform.Rotate (new Vector3 (x, y, z) * Time.deltaTime * speed);
	}
}
