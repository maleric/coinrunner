﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	public bool downButton=true;
	public float x;
	public float y;
	public bool jumped=false;
	
	private bool buttonsReady=false; 
	GUITexture up;
	GUITexture down;
	GUITexture left;
	GUITexture right;
	GUITexture jump;
	void Awake () {
		up=GameObject.FindGameObjectWithTag("ButtonUp").GetComponent<GUITexture>();
		left=GameObject.FindGameObjectWithTag("ButtonLeft").GetComponent<GUITexture>();
		right=GameObject.FindGameObjectWithTag("ButtonRight").GetComponent<GUITexture>();
		jump=GameObject.FindGameObjectWithTag("ButtonJump").GetComponent<GUITexture>();
		if(downButton){
			down=GameObject.FindGameObjectWithTag("ButtonDown").GetComponent<GUITexture>();
			if(up && down && left && right) buttonsReady = true;
		}
		else{
			down=null;
			if(up && left && right)buttonsReady = true;
		 }
	}

	void Update () {
		jumped=false;
		if(buttonsReady && Input.touchCount > 0){

			if (right.HitTest(Input.GetTouch(0).position))
			{ 
				CheckTouch("horizontal", 1); 
			}
			else if (left.HitTest(Input.GetTouch(0).position))
			{ 
				CheckTouch("horizontal", -1); 
			} 
			else if(!left.HitTest(Input.GetTouch(0).position) && !right.HitTest(Input.GetTouch(0).position))
			{
				ResetTouch("horizontal");
			}
			
			
			if (up.HitTest(Input.GetTouch(0).position))
			{ 
				CheckTouch("vertical", 1); 
			} 
			if (down && down.HitTest(Input.GetTouch(0).position))
			{ 
				CheckTouch("vertical", -1); 
			} 
			else if( (!down || !down.HitTest(Input.GetTouch(0).position))&&!up.HitTest(Input.GetTouch(0).position))
			{
				ResetTouch("vertical");
			}
			
			if(jump.HitTest(Input.GetTouch(0).position) 
				&& (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Began)){
			
				CheckTouch("vertical", 1); 
				jumped = true;
			}
		}
		else {
			x=0;
			y=0;
		}
	}
	void CheckTouch(string type, float value){
		if(type=="horizontal")		x=value;
		else if(type=="vertical")	y=value;
	 }
	 
	 void ResetTouch(string type){
		if(type=="horizontal")		x=0;
		else if(type=="vertical")	y=0;
	}

}
