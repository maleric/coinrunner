﻿using UnityEngine;
using System.Collections;

public class PlayerInteraction : MonoBehaviour {
	protected int count;
	public bool hasKey=false;
	public GUIText coinsText;
	public AudioClip coinsClip;
	public AudioClip keyClip;

	void Start () {
		count = 0;
		SetCountText();
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Pickup") {
			other.gameObject.SetActive(false);	
			AudioSource.PlayClipAtPoint(coinsClip, transform.position);
			count++;
			SetCountText();
		}
		if(other.gameObject.tag=="Key"){
			hasKey=true;
			other.gameObject.SetActive(false);	
			AudioSource.PlayClipAtPoint(keyClip, transform.position);	
		}
	}
	void SetCountText(){
		coinsText.text = count.ToString();
	}

}
