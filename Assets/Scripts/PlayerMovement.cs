using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {


	protected Animator animator;
	public bool end=false;
	public float directionDampTime = .25f;
	public bool isMobilePhone = true;
	private Movement movement; 
	private ulong delaySound=0;

	void Start () 
	{
		animator = GetComponent<Animator>();
		movement = GameObject.FindGameObjectWithTag("Movement").GetComponent<Movement>();
		if(animator.layerCount >= 2)
			animator.SetLayerWeight(1, 1);
	}	
	void FixedUpdate () 
	{	
		if (animator && !end)
		{
			AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);			
			
			float h;
			float v;
			bool jump=false;
			
			if(isMobilePhone){
				h=movement.x;
				v=movement.y;
				jump=movement.jumped;
			}
			
			else{
				h= Input.GetAxis("Horizontal");
				v = Input.GetAxis("Vertical");
				if (Input.GetButton("Fire1")) jump=true;
        	}
        	
        	
			if (stateInfo.IsName("Base Layer.Run"))
			{
				animator.SetBool("Jump", jump);          
			}
			else
			{
				animator.SetBool("Jump", false);                
			}
            
			MovementManagement(h, v);
			SoundManagement();
		}
		if(end) MovementManagement(0,0);
		if(end) audio.Stop();   		  
	}
	
	void MovementManagement (float horizontal, float vertical)	{
		
		if(vertical > 0f){
			animator.SetFloat("Speed", 5.5f, directionDampTime, Time.deltaTime);
			//transform.position+= new Vector3(10,0,0);
		}else if(vertical<0f){
			animator.SetFloat("Speed", -5.5f, directionDampTime, Time.deltaTime);
		}
		if(horizontal != 0f || vertical != 0f)
		{
			if(horizontal < 0)Rotate(-transform.right);
			if(horizontal > 0)Rotate(transform.right);
			horizontal=0;
			
		}
		else{
			animator.SetFloat("Speed", 0);
			}
	}
	void Rotate (Vector3 targetDirection, float angleSpeed=1.4f)	{
		Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
		Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, angleSpeed * Time.deltaTime);
		rigidbody.MoveRotation(newRotation);
	}
	void SoundManagement(){
		if(animator.GetFloat("Speed")>0){
			if(!audio.isPlaying){
				audio.Play(delaySound);
			}
		}
		else if(audio.isPlaying){ 
			audio.Stop();
			delaySound=0;
		}
		if(animator.GetBool("Jump") && audio.isPlaying){
			audio.Stop();
			delaySound=68000;
			}
	}
}
