﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
	public float timer=60f;
	private PlayerMovement player;
	private PlayerInteraction interactions;
	private float width=1920f;
	private float height=1080f;
	void Awake (){
		player=GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
		interactions=GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInteraction>();
	}
	
	void Update () {
		if(timer>=0){
			timer-=Time.deltaTime;
			guiText.text=timer.ToString("F0");	
		}else{
			player.end=true;
		}
	}
	
	void OnGUI () {
		GUI.matrix = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3(Screen.width/width, Screen.height/height, 1.0f));
		if(player.end){
			string message="GAME OVER";
			if(interactions.hasKey){
				message="COMPLETE";
			}
			GUIStyle style = GUI.skin.GetStyle ("label");
			GUIStyle buttonStyle = GUI.skin.GetStyle ("button");
			GUIStyle boxStyle = GUI.skin.GetStyle ("box");			
			style.fontSize = 54;
			buttonStyle.fontSize = 36;
			boxStyle.fontSize=36;
			GUI.Label (new Rect (width*0.41f, height*0.25f, width*0.4f, height*0.2f), message);
			GUI.Box(new Rect(width*0.20f,height*0.4f,width*0.6f,height*0.35f), "IZBORNIK");
	
			if(GUI.Button(new Rect(width*0.22f,height*0.45f,width*0.56f,height*0.1f), "Igraj ponovo" )) {
				Application.LoadLevel(1);
			}
			
			if(GUI.Button(new Rect(width*0.22f,height*0.60f,width*0.56f,height*0.1f), "Izlaz")) {
				Application.Quit();
			}
			GameObject movement = GameObject.FindGameObjectWithTag("Movement");
			movement.transform.position=new Vector3(999,0,0);
		}
	}
}
