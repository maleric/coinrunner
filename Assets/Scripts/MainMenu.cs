﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	void OnGUI () {
		
		GUIStyle style = GUI.skin.GetStyle ("button");
		style.fontSize=26 * ((int)Screen.width/800 * (int)Screen.height/800 );
		if(GUI.Button(new Rect(Screen.width*0.2f,Screen.height*0.5f-10,Screen.width*0.6f,Screen.height*0.1f), "Započni igru")) {
			Application.LoadLevel(1);
		}
		
		if(GUI.Button(new Rect(Screen.width*0.2f,Screen.height*0.6f,Screen.width*0.6f,Screen.height*0.1f), "Izlaz")){
			Application.Quit();
		}
	}
}
